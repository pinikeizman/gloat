export const fetchUserRepositoriesAndReturnCountOfStarsAndWatchers = user =>  {
    let pageCount = 0;
    let finished = false;
    let count = 0;
    return new Promise(resolve => {
    const numOfRepoPages = Math.ceil(user.public_repos/30);
    for(let i=1; i <= numOfRepoPages; i++){
        fetch("https://api.github.com/users/" + user.login + "/repos?page=" + i)
        .then(responseErrorFilter)
        .then( (repositories) => { 
            count += repositories.reduce( (sum, repo) => sum + repo.stargazers_count + repo.watchers_count, 0);
            if(++pageCount >= numOfRepoPages){
            resolve(count);
            }
         });
        }   
    });
}

export const searchUsersByName = query => fetch("https://api.github.com/search/users?q=" + query + "+in%3Alogin&type=Users")
.then( response => response.json() );

export const getUserProfile = (url) => fetch(url).then(response=>response.json());

const responseErrorFilter = (response) => {
    if(response.status != 200 && response.status != 204){
        throw new Error(response.body);
    }
    return response.json();
}
