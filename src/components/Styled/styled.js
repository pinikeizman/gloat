import styled from 'styled-components';

export const FlexBox = styled.div`
  display: flex;
  flex: ${props => (!props.flex ? '' : props.flex)};
  flex-direction: ${props => (!props.flexDirection ? '' : props.flexDirection)};
  justify-content: ${props =>
    !props.justifyContent ? '' : props.justifyContent};
  align-items: ${props => (!props.alignItems ? '' : props.alignItems)};
  align-self: ${props => (!props.alignSelf ? '' : props.alignSelf)};
  flex-basis: ${props => (!props.flexBasis ? '' : props.flexBasis)};
  height: ${props => (!props.height ? '' : props.height)};
  width: ${props => (!props.width ? '' : props.width)};
  padding: ${props => (!props.padding ? '' : props.padding)};
`;

export const StyledInput = styled.input`
  height: 30px;
  color: #333;
  width: 100%;
  font-size: 14px;
  font-family: Roboto;
  padding: 5px 10px;
  border: 1px solid #d3d3d3;
  box-sizing: border-box;
  :focus {
    outline: none;
    box-shadow: rgba(0, 0, 0, 0.5) 0px 0px 5px 0px;
  }
  :disabled {
    background: #dddddd;
  }
  ::placeholder {
    color: #c8c8c8;
    font-weight:100;
    opacity: 1; 
  }
`;

export const StyledTextArea = styled.textarea`
  width: 100%;
  color: #333;
  font-size: 14px;
  font-family: Roboto;
  padding: 5px 10px;
  border: none;
  border: 1px solid #d3d3d3;
  box-sizing: border-box;
  resize: none;
  :focus {
    outline: none;
    box-shadow: rgba(0, 0, 0, 0.5) 0px 0px 5px 0px;
  }
  :disabled {
    background: #dddddd;
  }
  ::placeholder {
    color: #c8c8c8;
    opacity: 1; 
  }
`;

export const StyledButton = styled.button`
  cursor: pointer;
  transition: background 0.5s ease;
  color: #fff;
  text-align: center;
  font-family: Roboto;
  font-size: 16px;
  background: ${props => props.color ? props.color :'#4aaadb'};
  height: 30px;
  border-radius: 5px;
  border: none;
  width: 100%;
  :hover {
    opacity: 0.8;
  }
  :focus {
    outline: none;
  }
  :active {
    transform: scale(0.95, 0.95);
  }
  :disabled {
    background: #dddddd;
  }
`;

export const StyledImage = styled.img`
  height: 50px;
  width: 50px;
  box-shadow: 0px 0px 3px 0px #8b8b8b;
  border-radius: 5px;
`;

export const StyledHeader = styled.h1`
  font-size: 24px;
  font-family: Roboto;
  margin: 10px 0px;
`;
export const StyledLabel = styled.label`
  font-size: 18px;
  font-family: Roboto;
  margin: 10px 0px;
`
export const StyledLink = styled.a`
  font-size: 1rem;

`
export default {
  FlexBox,
  StyledInput,
  StyledButton,
  StyledImage,
  StyledHeader,
  StyledLabel,
  StyledLink
};
