import React from "react";
import {FlexBox} from '../Styled/styled';
import styled from 'styled-components';

const Wrapper = styled(FlexBox)`
justify-content: space-between;
font-size: 1rem;
line-height: 1.4; 

.key{
  flex: 1;
}
.value{
  flex: 2;
  overflow-wrap: break-word;
  word-wrap: break-word;
  hyphens: auto;
  i{
    margin-right: 5px;
    min-width: 25px;
  }
}
  @media (max-width: 1008px) {
    flex-direction: column; 
    margin: 5px 0px;
    .value{
      padding-left: 5px;
    }
    .key{
      margin: 5px 0px;
    }
  }
`

const ResponsiveKeyValue = (props) => {
  return (
  <Wrapper className={'key-value-item' + props.className || ''}>
    <div className='key'>{props._key}</div>
    <FlexBox className='value'>
      {props.icon}
      {props.value}
    </FlexBox>
  </Wrapper>)

}
export default ResponsiveKeyValue;