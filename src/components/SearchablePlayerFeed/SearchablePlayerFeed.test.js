import React from 'react';
import ReactDOM from 'react-dom';
import CommentFeed from './CommentFeed';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CommentFeed comments={[{
    email: "pini1089@gmail.com",
    timestamp: "123456",
    text: "test",
    _id: 1231
  }]}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
