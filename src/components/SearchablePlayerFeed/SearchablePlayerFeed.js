import React from 'react';
import { FlexBox } from '../Styled/styled';
import styled,{ keyframes } from 'styled-components';
import * as api from '../../api';
import Select from 'react-select';
import PlayerFeed from '../PlayerFeed/PlayerFeed';

const greenFadeInOut = keyframes`
  0% {
    box-shadow: 0px 0px 5px rgba(0,255,0,0);
  }
  50% {
    box-shadow: 0px 0px 5px rgba(0,255,0,1);
  }
  100% {
    box-shadow: 0px 0px 5px rgba(0,255,0,0);
  }
`;

const redFadeInOut = keyframes`
  0% {
    box-shadow: 0px 0px 5px rgba(255,0,0,0);
  }
  50% {
    box-shadow: 0px 0px 5px rgba(255,0,0,1);
  }
  100% {
    box-shadow: 0px 0px 5px rgba(255,0,0,0);
  }
`;


export const Paper = styled(FlexBox)`
  height: 100%;
  border-radius: 5px;
  padding: 10px;
  margin: 10px;
  border: 1px solid rgba(0,0,0,0.3);
  flex: 1;
  ${  props => props.winner == undefined ? '' : 
        props.winner ? `animation: 2s ${greenFadeInOut} infinite ;` :
        `animation: 2s ${redFadeInOut} infinite ;` 
  };
  .player-feed{
    align-items: center;  
  }
`
const lastSearch = {}

/*
 * Github user selector with auto complete and user profile component.
 * */
export default class SearchablePlayerFeed extends React.Component{
  state = {
    options: []
  }

  handleSelectInputChange = (value) => {
    if(!value)
      return;
    
    this.setState({loading:true})
    this.searchUsers(value).then(users => this.setState({options: this.formatUsersToSelectOptions(users), loading: false}) );
  }

  formatUsersToSelectOptions = (users) => 
    users.map(user => ({ ...user, label: user.login, value: user.url }) );
    
  searchUsers = (query) => {
    if(lastSearch[query]){
      return new Promise(resolve => resolve(lastSearch[query]));
    }
    this.setState({loading:true});

    return api.searchUsersByName(query).then(users => {
      this.setState({loading: false});
      lastSearch[query] = users.items;
      return users.items.sort( (userA, userB) => {
        return userA.login.length - userB.login.length;
      });
    });
  }


  render(){
    const {winner, handleSelectChange, playerId, player, loadingUserProfile} = this.props;
    const { options, loading } = this.state;
    return( 
      <Paper flexDirection='column' winner={winner} >
        <Select className={"select"}
                isLoading={loading}
                onInputChange={winner != undefined ? undefined : this.handleSelectInputChange} 
                options={options} 
                onChange={winner != undefined ? undefined : (option)=> handleSelectChange(option, playerId)}/>
        <PlayerFeed 
              player={player} 
              loading={loadingUserProfile} 
              winner={winner}/>
      </Paper>
    )
  }
};

