import React from 'react';
import { FlexBox } from '../Styled/styled';
import styled,{keyframes} from 'styled-components';

const rotate = keyframes`
  0%{
    transform: rotate(0deg);
  }
  50%{
    transform: rotate(180deg);
  }
  100%{
    transform: rotate(360deg);
  }
`

const Spinner = styled.i`
  position: absolute;
  animation: 2s ${rotate} infinite;
  font-size: ${props => `${props.fontSize}px`};
`


const Loading = (props) => {

    return( 
      <FlexBox  
        padding={props.padding}
        height={`${props.size}px`}
        justifyContent="center"
        alignItems="center">
        <Spinner fontSize={props.size} className={"fas fa-spinner "} ></Spinner>
      </FlexBox>
    )
};

export default Loading;

