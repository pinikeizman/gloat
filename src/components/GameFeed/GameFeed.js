import React from 'react';
import { FlexBox, StyledHeader, StyledLabel , StyledButton} from '../Styled/styled';
import SearchablePlayerFeed from '../SearchablePlayerFeed/SearchablePlayerFeed';
import FightSummaryPopUp from '../FightSummaryPopUp/FightSummaryPopUp';
import * as api from '../../api';
import styled from 'styled-components';
import _ from 'lodash';

const ResponsiveFeed = styled(FlexBox)`
@media (max-width: 1008px) {
  .player-profile{
    min-height: 200px;
    max-height: 200px;
    overflow: scroll;
  }
}
@media (max-width: 460px) {
    flex-direction: column; 
    .player-profile{
      min-height: 200px;
      max-height: 200px;
      overflow: scroll;
    }
  }
`

const playerAId = 'playerA';
const playerBId = 'playerB';

const initialState = {
  options: [],
  [playerAId]: {},
  [playerBId]: {},
  winner: undefined,
  popup: {}
};

export default class GameFeed extends React.Component{
  state = initialState;
  cachedUsers = {}

  getUserData = api.getUserProfile;

  handleSelectChange = (option, playerId) => {
    if(option.length == 0)
      return;

    if(!this.cachedUsers[option.label]){
      this.setState({ [playerId]: { loading: true } });
      return this.getUserData(option.value).then(user =>{
        this.cachedUsers[option.label] = user;
        this.setState({ [playerId]: { userId: option.label } });
      });
    }

    this.setState({ [playerId]: { userId: option.label } });
    return this.cachedUsers[option.label];
}

  fight = (userA, userB) => {
    const popup = {
      open: true
    }

    if(_.isEmpty(userA) || _.isEmpty(userB) )
    return;

    this.setState({ winner, loadingFight: true });

    let winner = this.compareUsersScore(userA.score, userB.score, {aWinText: userA.login, bWinText: userB.login, tieText: 'tie'});
    if(winner){
      return this.setState({ winner, loadingFight: false, popup });
    }

    if(userA.score == undefined){
      this.countUserStarsAndWatchers(userA).then(res => {
        // cahce scor in user object
        userA.score = res;
        this.cachedUsers[userA.login] = userA;
        winner = this.compareUsersScore(userA.score, userB.score, {aWinText: userA.login, bWinText: userB.login, tieText: 'tie'});
        if(winner){
          return this.setState({winner, loadingFight: false, popup })
        }
      });
    }

    if(userB.score == undefined){
      this.countUserStarsAndWatchers(userB).then(res => {
        // cahce scor in user object
        userB.score = res;
        this.cachedUsers[userB.login] = userB;
        winner = this.compareUsersScore(userA.score, userB.score, {aWinText: userA.login, bWinText: userB.login, tieText: 'tie'});
        if(winner){
          return this.setState({winner, loadingFight: false ,popup })
        }
      });
    }

  }

  compareUsersScore = (userAscore, userBscore, options = {}) => {
    if(userAscore == undefined || userBscore == undefined){
      return undefined;
    }
    if(userAscore < userBscore){
      return options.bWinText || -1;
    }else if(userAscore > userBscore){
      return options.aWinText || 1;
    }else if(userAscore == userBscore){
      return options.tieText || 0;
    }

  }

  countUserStarsAndWatchers = api.fetchUserRepositoriesAndReturnCountOfStarsAndWatchers;

  render(){
    const {cachedUsers} = this;
    const {winner, playerA, playerB, loadingFight, popup} = this.state;
    const userA = cachedUsers[playerA.userId] || {};
    const userB = cachedUsers[playerB.userId] || {};

    return( 
      <div>
        <StyledHeader>GitHub Battle</StyledHeader>
        <ResponsiveFeed justifyContent="space-between">
          <SearchablePlayerFeed 
              playerId={"playerA"}
              handleSelectChange={this.handleSelectChange}  
              className="player-feed"
              player={userA} 
              loadingUserProfile={playerA.loading} 
              winner={winner == undefined ? undefined : winner == userA.login}/>

          <SearchablePlayerFeed 
              playerId={"playerB"}
              handleSelectChange={this.handleSelectChange} 
              className="player-feed"
              player={userB} 
              loadingUserProfile={playerB.loading} 
              winner={winner == undefined ? undefined : winner == userB.login}/>

        </ResponsiveFeed>
        <StyledButton disabled={loadingFight}  color={winner ? "#BBB" : undefined } onClick={winner ? ()=>this.setState(initialState) :  () => this.fight(userA, userB)}>
          {winner ? "Reset" : "Fight" }
        </StyledButton>
        <FightSummaryPopUp 
          open={winner != undefined} 
          winner={userA.login == winner ? userA : userB} 
          looser={userA.login == winner ? userB : userA} 
          closeModel={popup.closeModel}/>
      </div>)
  }
};

