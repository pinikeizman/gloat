

import React from "react";
import {FlexBox, StyledImage} from '../Styled/styled';
import styled from 'styled-components';
import Popup from 'reactjs-popup';

const StyledPopUp = styled(FlexBox)`
  margin: 15px;
  .player-summary{
    padding: 10px;
    *{
      margin: 5px 0px;
    }
  }
  @media (max-width: 600px) {
    .summary-winner{
      flex-direction: column;
    }
  }
  .summary-winner .label{
    margin: 0px 20px;
    font-size: 20px;
    font-weight: bold;
    text-align: center;
  }
  .summary-winner .score{
    margin: 0px 20px;
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    color: green;
  }
`

const FightSummaryPopUp = (props) => {
  const { open, closeModal, winner, looser } =  props;
  return (
    <Popup
      open={open}
      closeOnDocumentClick
      onClose={closeModal}>
      <StyledPopUp flexDirection='column' alignItems="center">
        <FlexBox justifyContent="space-between"  flex={1} width="100%">
          <FlexBox className="player-summary winner" alignItems={"center"} flexDirection='column'>
              <StyledImage 
                src={winner.avatar_url} 
                alt={winner.login}/>
                <div>
                  <div className="label">{winner.login|| "N/A"}</div>
                  <div>{winner.score|| "N/A"}</div>
                </div>
          </FlexBox>
          VS.
          <FlexBox className="player-summary looser" alignItems={"center"} flexDirection='column'>
              <StyledImage 
                src={looser.avatar_url} 
                alt={looser.login}/>
            <div>
              <div>{looser.login || "N/A"}</div>
              <div>{looser.score || "N/A"}</div>
            </div>
          </FlexBox>
        </FlexBox>
        <FlexBox className="summary-winner">
          <div className="label">{winner.login || "N/A"}</div>
          <div>Win's with score of:</div>
          <div className="score">{winner.score || "N/A"}</div>
        </FlexBox>
      </StyledPopUp>
    </Popup>
  )
}

export default FightSummaryPopUp;