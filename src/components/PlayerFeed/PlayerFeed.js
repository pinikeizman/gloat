import React from 'react';
import { FlexBox, StyledLabel, StyledImage, StyledLink } from '../Styled/styled';
import ResponsiveKeyValue from "../ResponsiveKeyValue/ResponsiveKeyValue";
import styled from 'styled-components';
import moment from 'moment';
import Loading from '../Loading/Loading';

const StyledPlayerFeed = styled(FlexBox)`
  transition: height 1s ease-out;
`;

const fields = {
  html_url: {
    label: "GitHub link:",
    icon: <i className="fab fa-github"></i>
  },
  followers: {
    label: "Followers",
    icon: <i className="fas fa-clipboard-check"></i>
  },
  company: {
    label: "Name of company",
    icon: <i className="far fa-building"></i>
  },
  location: {
    label: "Location",
    icon: <i className="fas fa-location-arrow"></i>
  },
  bio: {
    label: "Bio",
    icon: <i className="fas fa-info"></i>
  },  
  created_at:{
    label: "Github join date",
    type: "date",
    icon: <i className="fas fa-plus"></i>
  },
  following: {
    label: "# of following",
    icon: <i className="fas fa-users"></i>
  },
  public_repos: {
    label: "# of public repos",
    icon: <i className="fas fa-folder-open"></i>
  }
}

/*
 *  Github user profile UI component.
 * */
const PlayerFeed = (props) => {
    const {winner, player, loading} = props;

    if(loading){
      return <Loading size={24} padding="20px 0px"/>;
    }

    if(!player || Object.keys(player) == 0){
      return <div/>;
    }

    return( 
      <StyledPlayerFeed 
              winner={winner}
              flexDirection='column'
              flex='1'
              alginItems="center">
          <StyledImage style={{margin: '20px 0px 0px 0px'}} src={player.avatar_url}/>
          <StyledLabel style={{margin: '10px 0px 20px 0px'}} className='player-label'>{player.name}</StyledLabel>
          <div className="player-profile">
        {
            player && Object
                        .keys(player)
                        .filter(field => !!fields[field])
                        .map(key => 
                        <ResponsiveKeyValue key={key} 
                                            icon={(fields[key] || {}).icon}
                                            _key={(fields[key] || {}).label}
                                            value={(fields[key].type == "date" && moment(player[key]).format('MMMM Do YYYY') || player[key] || 'N/A')}/>)
        }
        </div>
      </StyledPlayerFeed>
    )
  };
  export default PlayerFeed;

